# goDE

A differential evolution experiment written in golang.


### Important

This is a work in progress, the project itself has two purposes.
  - Learning how to use golang efficiently
  - Making available the packages containing the implementation of the differential evolution.
